package com.abms.restrosofttabsystem.myAsynkASP;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.abms.restrosofttabsystem.CustomAdapters.GetandSet;
import com.abms.restrosofttabsystem.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CategoryAsynk extends AsyncTask<String, String, String> {

    String status = null;
    Activity activity;
    String catId,catName;
    GetandSet dataDTO;
    ProgressDialog pDialog = null;
    Spinner spnCategory;
    TextView txtdate;

    private List<GetandSet> dataList = new ArrayList<>();

    // For checking login authentication
    public CategoryAsynk(Activity act, Spinner spnCategory,TextView txtdate) {
        this.activity = act;
        this.spnCategory = spnCategory;
        this. txtdate = txtdate;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Loading data.."); // typically you will define such
        // strings in a remote file.
        pDialog.show();
    }

    @Override
    protected String doInBackground(String... connUrl) {
        HttpURLConnection conn = null;
        BufferedReader reader;

        try {
            final URL url = new URL(connUrl[0]);
            conn = (HttpURLConnection) url.openConnection();
            conn.addRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestMethod("GET");
            int result = conn.getResponseCode();
            if (result == 200) {

                InputStream in = new BufferedInputStream(conn.getInputStream());
                reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    status = line;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return status;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }

        if (result != null)
            try {

                JSONArray jsonArray = new JSONArray(result);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    final String cat_id = object.getString("Cat_id");
                    final String cat_Name = object.getString("Cat_name");
                    final String dDate = object.getString("Ddate");

                    dataDTO = new GetandSet();
                    dataDTO.setCat_id(cat_id);
                    catId = dataDTO.getCat_id().toString();

                    dataDTO.setCat_name(cat_Name);
                    catName = dataDTO.getCat_name().toString();

                    dataDTO.setDate(dDate);
                    txtdate.setText(dataDTO.getDate().toString());

                    dataList.add(dataDTO);

                }

                List<String> categorylable = new ArrayList<String>();
                categorylable.add("Select Category");
                for(int i = 0; i<dataList.size(); i++)
                {
                    categorylable.add(dataList.get(i).getCat_name().toString());
                }

                ArrayAdapter<String> adp2 = new ArrayAdapter<String>
                        (activity,android.R.layout.simple_list_item_1,categorylable);
                adp2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spnCategory.setAdapter(adp2); // spn category



            } catch (NullPointerException e){
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        else {
            Toast.makeText(activity, "Could not get category data.", Toast.LENGTH_LONG).show();
        }
    }

    public String wDetails()
    {
        return String.valueOf(dataDTO);
    }
}