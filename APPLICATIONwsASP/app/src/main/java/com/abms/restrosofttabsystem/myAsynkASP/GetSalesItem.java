package com.abms.restrosofttabsystem.myAsynkASP;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.abms.restrosofttabsystem.CustomAdapters.CustomAdapter_OrderDetails;
import com.abms.restrosofttabsystem.CustomAdapters.GetandSet;
import com.abms.restrosofttabsystem.MainModule.DashBoard;
import com.abms.restrosofttabsystem.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class GetSalesItem extends AsyncTask<String, String, String> {
    String status = null;
    ProgressDialog pDialog;
    Activity activity;
    GetandSet dataDTO;
    ListView lstData;
    TextView txtgrandtotal;
    String wid,tid;
    String order_id,menu_id, m_name,rate,qty,total_amount;
    CustomAdapter_OrderDetails customadapter;

    //CancleOrder cancleOrder;
    private List<GetandSet> dataList = new ArrayList<>();
    // For checking login authentication
    public GetSalesItem(Activity act, ListView listView, TextView txtgdtotal,String wid, String tid) {
        this.activity = act;
        this.lstData = listView;
        this.txtgrandtotal = txtgdtotal ;
        this.wid = wid;
        this.tid = tid;
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Loading data.."); // typically you will define such
        // strings in a remote file.
        pDialog.show();

    }

    @Override
    protected String doInBackground(String... connUrl) {
        HttpURLConnection conn = null;
        BufferedReader reader;

        try {
            final URL url = new URL(connUrl[0]);
            conn = (HttpURLConnection) url.openConnection();
            conn.addRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestMethod("GET");
            int result = conn.getResponseCode();
            if (result == 200) {

                InputStream in = new BufferedInputStream(conn.getInputStream());
                reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    status = line;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return status;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }

        if (result != null)
            try {

             JSONArray jsonArray = new JSONArray(result);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                     menu_id      = object.getString("Menu_id");
                     order_id      = object.getString("Order_id");
                     qty          = object.getString("Qty");
                     rate         = object.getString("Rate");
                     total_amount = object.getString("Total_amount");
                     m_name       = object.getString("M_name");

                    dataDTO = new GetandSet();

                    dataDTO.setOrder_id(order_id);
                    dataDTO.setMenu_id(menu_id);
                    dataDTO.setRate(rate);
                    dataDTO.setM_name(m_name);

                    dataDTO.setMenu_qty(qty);
                    dataDTO.setMenu_price(Float.parseFloat(total_amount));

                    dataList.add(dataDTO);
                }

            customadapter = new CustomAdapter_OrderDetails(activity,activity,dataList,txtgrandtotal);
            lstData.setAdapter(customadapter);
            customadapter.notifyDataSetChanged();

            int count = lstData.getCount();
            String grandtotal = txtgrandtotal.getText().toString();
            String WID = wid;
            String TID = tid;

//            if(count < 1 || count == 0)
//            {
//                cancleOrder = new CancleOrder(activity,grandtotal,OrderID,TID,WID,"No Order");
//                cancleOrder.execute();
//            }

            } catch (NullPointerException e)
            {
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        else {
            Toast.makeText(activity, "Could not get any data.", Toast.LENGTH_LONG).show();
        }
    }

        public ArrayList<GetandSet> listdetails()
    {
        return (ArrayList<GetandSet>) dataList;
    }
}