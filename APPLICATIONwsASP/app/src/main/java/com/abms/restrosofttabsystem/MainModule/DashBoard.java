package com.abms.restrosofttabsystem.MainModule;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

//import com.abms.restrosofttabsystem.Asynctask.GetProcessOrderData;
import com.abms.restrosofttabsystem.Database.Common;
import com.abms.restrosofttabsystem.Database.ConfigIP;
import com.abms.restrosofttabsystem.Database.Database;
import com.abms.restrosofttabsystem.R;
import com.abms.restrosofttabsystem.myAsynkASP.SelectAsynk;
import com.abms.restrosofttabsystem.myAsynkASP.TableStatusAsynk;
//import com.airbnb.lottie.LottieAnimationView;

import java.util.List;


public class DashBoard extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    TextView txtWaiterName,txtWname;
    Button btnNewtable;
    String wname,waiterid;
    ListView processorderList;
    NavigationView navigationView ;
    Bundle bu;
    SwipeRefreshLayout refreshLayout;
    Database database;
    ConfigIP configIP;
//    LottieAnimationView lottieAnimationView;
    TableStatusAsynk tableStatusAsynk;

//    ImageView img_no_order;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        final String ip = new Common(DashBoard.this).getIpfromDatabase();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        database = new Database(getApplicationContext(),null,null,1);
        configIP = new ConfigIP();

        txtWaiterName = (TextView)findViewById(R.id.txt_WaiterName);
        txtWname = (TextView)navigationView.getHeaderView(0).findViewById(R.id.txt_W_Name);
        btnNewtable = (Button)findViewById(R.id.btn_NewOrder);
        refreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh_layout);

        processorderList = (ListView)findViewById(R.id.list_ProcessOrder);

        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        bu = new Bundle();
        bu = getIntent().getExtras();

        if (bu != null) {
            //String extraStr = extras.getString("extra");

            wname = bu.getString("waitername");
            waiterid = bu.getString("waiterid");

            txtWaiterName.setText(wname);
            txtWname.setText(wname);

        }else {
            SharedPreferences preferences = getPreferences(MODE_PRIVATE);
            wname = preferences.getString("waiter_name","0");
            waiterid = preferences.getString("waiter_Id","0");
            txtWaiterName.setText(wname);
            txtWname.setText(wname);
        }

        btnNewtable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashBoard.this,NewTable.class);
                intent.putExtra("waiterid",waiterid);
                startActivity(intent);
            }
        });


        refreshLayout.setColorSchemeResources(R.color.colorPrimary,
        android.R.color.holo_green_light,
        android.R.color.holo_orange_dark,
        android.R.color.holo_blue_dark);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
           tableStatusAsynk = new TableStatusAsynk(DashBoard.this, processorderList,waiterid);
           tableStatusAsynk.execute(ip+"/"+"Service1.svc"+"/"+DashBoard.this.getString(R.string.getTableStatus)+"/"+waiterid);
            refreshLayout.setRefreshing(false);

        }
    });
    //Default Load
        refreshLayout.post(new Runnable() {
        @Override
        public void run() {

            tableStatusAsynk = new TableStatusAsynk(DashBoard.this, processorderList,waiterid);
            tableStatusAsynk.execute(ip+"/"+"Service1.svc"+"/"+DashBoard.this.getString(R.string.getTableStatus)+"/"+waiterid);
           // new TableStatusAsynk(DashBoard.this, processorderList,waiterid).execute(ip+"/"+"Service1.svc"+"/"+DashBoard.this.getString(R.string.getTableStatus)+"/"+waiterid);
            refreshLayout.setRefreshing(false);

        }
    });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);

            Intent startmain = new Intent(Intent.ACTION_MAIN);
            startmain.addCategory(Intent.CATEGORY_HOME);
            startmain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startmain);
        } else {
            //super.onBackPressed();

            Intent startmain = new Intent(Intent.ACTION_MAIN);
            startmain.addCategory(Intent.CATEGORY_HOME);
            startmain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startmain);

        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dash_board, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            showaddipdialog();
           // return true;
        }
        if (id == R.id.action_Logout) {
            new AlertDialog.Builder(DashBoard.this)
                        .setTitle("LogOut")
                        .setMessage("Do you want to Logout?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                // Toast.makeText(DashBoard.this, "Yaay", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(DashBoard.this,Login.class));
                            }})
                        .setNegativeButton(android.R.string.no, null).show();
        }

        return super.onOptionsItemSelected(item);
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_newTable) {
            Intent intent = new Intent(DashBoard.this,NewTable.class);
            intent.putExtra("waiterid",waiterid); // putting data to new table
            startActivity(intent);

            // Handle the camera action
        }  else if (id == R.id.nav_CaptainDetails) {

            startActivity(new Intent(DashBoard.this, CaptainDetails.class));
            

        } else if (id == R.id.nav_settings) {
            showaddipdialog();

        } else if (id == R.id.nav_Logout) {
            new AlertDialog.Builder(DashBoard.this)
                    .setTitle("LogOut")
                    .setMessage("Do you want to Logout?")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            // Toast.makeText(DashBoard.this, "Yaay", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(DashBoard.this,Login.class));
                        }})
                    .setNegativeButton(android.R.string.no, null).show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @Override
    protected void onPause()
    {
        super.onPause();

        // Store values between instances here
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();  // Put the values from the UI

        String strName = txtWaiterName.getText().toString();
        String strid =  waiterid;

        editor.putString("waiter_name", strName); // value to store
        editor.putString("waiter_Id", strid); // value to store

        // Commit to storage
        editor.commit();
    }

    private void showaddipdialog() {

        android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(DashBoard.this);
        alertDialog.setTitle("Set Ip");
        alertDialog.setMessage("Please Fill all Information");

        LayoutInflater layoutInflater = this.getLayoutInflater();
        View changePwdLayout = layoutInflater.inflate(R.layout.ipalertdialog, null);

        final EditText edtip, edthost;
        edtip = (EditText) changePwdLayout.findViewById(R.id.edt_ip);
        edthost = (EditText) changePwdLayout.findViewById(R.id.edt_host);

        alertDialog.setView(changePwdLayout);

        final Boolean isExist = database.checkipExist();
        if (isExist) {
            List<ConfigIP> get = database.getAllContacts();
            for (ConfigIP cn : get) {
                edtip.setText(cn.getIp());
                edthost.setText(cn.getHost());
            }
        }else
        {
            edtip.setText("");
            edthost.setText("");
        }

            //Set Buttons
            alertDialog.setPositiveButton("Set", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    if (!isExist) {
                        configIP.setIp(edtip.getText().toString());
                        configIP.setHost(edthost.getText().toString());

                        database.addDetails(configIP);
                        Toast.makeText(getApplicationContext(), "Successfully Configured Ip", Toast.LENGTH_LONG).show();
                    } else {
                        database.updateip(edtip.getText().toString(), edthost.getText().toString());
                        Toast.makeText(getApplicationContext(), "Successfully Configured Ip", Toast.LENGTH_LONG).show();

                    }
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
    }

}
