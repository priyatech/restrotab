package com.abms.restrosofttabsystem.myAsynkASP;


import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.abms.restrosofttabsystem.CustomAdapters.GetandSet;
import com.abms.restrosofttabsystem.MainModule.ReOrder;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class InsertAsynk extends AsyncTask<String, String, String> {

    ArrayList<GetandSet> ItemsDetails;
    ArrayList<GetandSet> tempstore = new ArrayList<GetandSet>();
    String result;

    String m_name,menu_id,rate,desc,total_amount,qty;
//    int qty;
//    float total_amount;

    String t_id,timeing,w_id,order_type = "novat",tableStatus,orderStartTime,status= null;
    Activity activity;

//    String responseStr;
//    String order_id,Kot_id,date;
//
//    String category;
//    GetandSet getandSet;
    String guest;

    //Activity activity,String OrderId, String date, String wid, ArrayList<GetandSet> itemlist

    public InsertAsynk(Activity activity,String tid,String date,String wid,String odty,String ts,String otime, String guest, ArrayList<GetandSet> itemlist) { //, ArrayList<GetandSet> itemlist
            this.activity = activity;
            this.t_id = tid;
            this.timeing = date; // same date
            this.w_id  = wid;
            this.order_type = odty;
            this.tableStatus = ts;
            this.orderStartTime = otime;
            this.guest = guest;
            this.ItemsDetails = itemlist;
    }

    protected void onPreExecute(){}
    protected String doInBackground(String... connUrl){
        HttpURLConnection conn=null;
        BufferedReader reader;

        try{
            final URL url = new URL(connUrl[0]);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setChunkedStreamingMode(0);

            conn.addRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestMethod("POST");

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("t_id",t_id);  //object name is case sensEtive. it must be same as service parameter.
            jsonObject.put("timeing",timeing);
            jsonObject.put("w_id",w_id);
            jsonObject.put("order_type",order_type);
            jsonObject.put("tableStatus",tableStatus);
            jsonObject.put("guest",guest);
            //jsonObject.put("order_id",order_id);

            for(int i = 0; i < ItemsDetails.size();i++) {

                GetandSet add = new GetandSet();

                    menu_id      = ItemsDetails.get(i).getMenu_id();
                    qty          = ItemsDetails.get(i).getMenu_qty();
                    rate         = ItemsDetails.get(i).getRate();
                    total_amount = String.valueOf(ItemsDetails.get(i).getMenu_price());
                    m_name       = ItemsDetails.get(i).getM_name();
                    desc         = ItemsDetails.get(i).getDes();
                    add.setMenu_id(menu_id);
                    add.setM_name(m_name);
                    add.setRate(rate);
                    add.setMenu_qty(qty);
                    add.setMenu_price(Float.parseFloat(total_amount));
                    add.setDes(desc);
                    tempstore.add(add);
            }

            String stringToPost = new Gson().toJson(tempstore);
            jsonObject.put("jsons",stringToPost);

            OutputStream out = new BufferedOutputStream(conn.getOutputStream());
            out.write(jsonObject.toString().getBytes());
            out.flush();
            out.close();

            int result = conn.getResponseCode();
            System.out.println(""+conn.getResponseCode() + ": " + conn.getResponseMessage());
            System.out.println("Response" + " " + result);
            System.out.println(jsonObject.toString());

            if(result==200){
                InputStream in = new BufferedInputStream(conn.getInputStream());
                reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while((line = reader.readLine())!=null){
                    status = line;
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return status;
    }
    protected void onPostExecute(String result){
        super.onPostExecute(result);
        Log.d("Order Id",result);
        Intent intent = new Intent(activity, ReOrder.class);
        intent.putExtra("tableNo",t_id);
        intent.putExtra("orderid",result);
        intent.putExtra("waiterid",w_id);
        activity.startActivity(intent);
        if(result!=null){
            Toast.makeText(activity,"Order Saved Successfuly.",Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(activity,"Doesn't Saved Data.",Toast.LENGTH_LONG).show();
        }
    }
        public String orderid(){return result;}
}