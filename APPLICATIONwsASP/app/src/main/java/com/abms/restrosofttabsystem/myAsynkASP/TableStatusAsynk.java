package com.abms.restrosofttabsystem.myAsynkASP;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.abms.restrosofttabsystem.CustomAdapters.CustomAdapter_ProcessOrder;
import com.abms.restrosofttabsystem.CustomAdapters.GetandSet;
import com.abms.restrosofttabsystem.MainModule.DashBoard;
import com.abms.restrosofttabsystem.MainModule.ReOrder;
import com.abms.restrosofttabsystem.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


// ip+"/"+"Service1.svc"+"/"+DashBoard.this.getString(R.string.getTableStatus)+"/"+waiterid in dashboard in refreshlayout

public class TableStatusAsynk extends AsyncTask<String, String, String> {

    String status = null;
    Activity activity;
    String userid,t_id,statusTable,orderId;
    String waiterid;
    GetandSet dataDTO;
    ProgressDialog pDialog = null;
    CustomAdapter_ProcessOrder customadapter;
    ListView listView;

    private List<GetandSet> dataList = new ArrayList<>();

    // For checking login authentication
    public TableStatusAsynk(Activity act,ListView listView,String w_id) {
        this.activity = act;
        this.listView = listView;
        this.waiterid = w_id;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Loading data.."); // typically you will define such
        // strings in a remote file.
        pDialog.show();

    }

    @Override
    protected String doInBackground(String... connUrl) {
        HttpURLConnection conn = null;
        BufferedReader reader;

        try {
            final URL url = new URL(connUrl[0]);
            conn = (HttpURLConnection) url.openConnection();
            conn.addRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestMethod("GET");
            int result = conn.getResponseCode();
            if (result == 200) {

                InputStream in = new BufferedInputStream(conn.getInputStream());
                reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    status = line;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return status;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }

        if (result != null)
            try {

                JSONArray jsonArray = new JSONArray(result);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);

                    // new Edition
                        String order_id = object.getString("Order_id");
                        String table_id = object.getString("T_id");
                        String tblStatus = object.getString("Table_Status");

                    // new Edition

                    dataDTO = new GetandSet();

                        dataDTO.setOrder_id(order_id);
                        orderId = dataDTO.getOrder_id().toString();

                        dataDTO.setT_id(table_id);
                        t_id = dataDTO.getT_id().toString();

                        dataDTO.setStatus(tblStatus);
                        statusTable = dataDTO.getStatus().toString();

                    dataList.add(dataDTO);

                }

                    // for table status to display in list view dashboard
                    customadapter = new CustomAdapter_ProcessOrder(activity,dataList);
                    listView.setAdapter(customadapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent i= new Intent(activity,ReOrder.class);
                            String tableNo = dataList.get(position).getT_id();
                            i.putExtra("tableNo",tableNo);
                            i.putExtra("orderid",dataList.get(position).getOrder_id());
                            i.putExtra("waiterid",waiterid);
                            activity.startActivity(i);
                        }
                    });

            } catch (NullPointerException e)
            {

                Toast toast = new Toast(activity);
                toast.setDuration(Toast.LENGTH_LONG);

                LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inflater.inflate(R.layout.your_custom_layout, null);
                toast.setView(view);
                toast.show();


            } catch (Exception ex) {
                ex.printStackTrace();
            }
        else {
            Toast.makeText(activity, "Could not get any data.", Toast.LENGTH_LONG).show();
        }
    }

    public String wDetails()
    {
        return String.valueOf(dataList);
    }
}