package com.abms.restrosofttabsystem.myAsynkASP;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.abms.restrosofttabsystem.CustomAdapters.GetandSet;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MenuAsynk extends AsyncTask<String, String, String> {

    String status = null;
    Activity activity;
    GetandSet dataDTO;
    ProgressDialog pDialog = null;
    Spinner spnMenu;
    String category, table_id;

    private List<GetandSet> dataList = new ArrayList<>();
    private List<GetandSet> menuitems = new ArrayList<>();

    public MenuAsynk(Activity activity, Spinner spnMenu, String category, String table_id) {
        this.activity = activity;
        this.spnMenu = spnMenu;
        this.category = category;
        this.table_id = table_id;
    }

    // For checking login authentication

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Loading data.."); // typically you will define such
        // strings in a remote file.
        pDialog.show();
    }

    @Override
    protected String doInBackground(String... connUrl) {
        HttpURLConnection conn = null;
        BufferedReader reader;

        try {
            final URL url = new URL(connUrl[0]);
            conn = (HttpURLConnection) url.openConnection();
            conn.addRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestMethod("GET");
            int result = conn.getResponseCode();
            if (result == 200) {

                InputStream in = new BufferedInputStream(conn.getInputStream());
                reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    status = line;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return status;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }

        if (result != null)
            try {

                JSONArray jsonArray = new JSONArray(result);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    final String menu_id = object.getString("M_id");
                    final String menu_Name = object.getString("M_name");
                    final String menu_rate = object.getString("M_rate");

                    dataDTO = new GetandSet();

                    dataDTO.setMenu_id(menu_id);
                    dataDTO.setM_name(menu_Name);
                    dataDTO.setRate(menu_rate);

                    dataList.add(dataDTO);

                }

                List<String> menulable = new ArrayList<String>();
                menulable.add("Select Menu Item");
                for(int i = 0; i<dataList.size(); i++)
                {
                    menulable.add(dataList.get(i).getM_name().toString());
                }
                ArrayAdapter<String> adp3 = new ArrayAdapter<String>
                        (activity,android.R.layout.simple_list_item_1,menulable);
                adp3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spnMenu.setAdapter(adp3);



            } catch (NullPointerException e){
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        else {
            Toast.makeText(activity, "Could not get menu.", Toast.LENGTH_LONG).show();
        }
    }

    public ArrayList<GetandSet> details()
    {
        return (ArrayList<GetandSet>) dataList;
    }
}