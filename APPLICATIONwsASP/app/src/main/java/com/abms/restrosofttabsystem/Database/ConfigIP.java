package com.abms.restrosofttabsystem.Database;

public class ConfigIP {
    public String ip,host;
    public String wId,wName,wAddress,wPassword,wMobile;

    public ConfigIP(String ip, String host) {
        this.ip = ip;
        this.host = host;
    }

    public ConfigIP() {
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }


    public String getwId() {
        return wId;
    }

    public void setwId(String wId) {
        this.wId = wId;
    }

    public String getwName() {
        return wName;
    }

    public void setwName(String wName) {
        this.wName = wName;
    }

    public String getwAddress() {
        return wAddress;
    }

    public void setwAddress(String wAddress) {
        this.wAddress = wAddress;
    }

    public String getwPassword() {
        return wPassword;
    }

    public void setwPassword(String wPassword) {
        this.wPassword = wPassword;
    }

    public String getwMobile() {
        return wMobile;
    }

    public void setwMobile(String wMobile) {
        this.wMobile = wMobile;
    }
}
