package com.abms.restrosofttabsystem.CustomAdapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.abms.restrosofttabsystem.R;

import java.util.List;

/**
 * Created by Ndroid1 on 13/03/2018.
 */

public class CustomAdapter_ProcessOrder extends BaseAdapter {

    Activity activity;
    Context CONTEXT;
    LayoutInflater layoutInflater;
    private List<GetandSet> details;
    ViewHolder viewHolder;

    public CustomAdapter_ProcessOrder(Context context, List<GetandSet> details) {

        this.CONTEXT = context;
        this.details = details;

        this.layoutInflater = (LayoutInflater) LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return details.size();
    }

    @Override
    public Object getItem(int position) {
        return details.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final GetandSet recentItem = details.get(position);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater)CONTEXT.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.custom_process_order_list, null);
            //convertView = layoutInflater.inflate(R.layout.custom_adapter_template, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.txtTableNo.setText(details.get(position).getT_id());
        viewHolder.txtOrderId.setText(details.get(position).getOrder_id());
        viewHolder.txtorderstatus.setText(details.get(position).getStatus());
        viewHolder.txttime.setText(details.get(position).getOrdertime());

        return convertView;
    }

    private class ViewHolder {
        TextView txtTableNo,txtOrderId, txttime,txtorderstatus;

        public ViewHolder(View view)
        {
            txtTableNo = (TextView) view.findViewById(R.id.txt_TableNo);
            txtOrderId = (TextView) view.findViewById(R.id.txt_OrderID);
            txtorderstatus = (TextView) view.findViewById(R.id.txt_Orderstatus);
            txttime = (TextView) view.findViewById(R.id.txt_OrderTime);
        }
    }
}
