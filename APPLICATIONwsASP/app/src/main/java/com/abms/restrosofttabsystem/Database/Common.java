package com.abms.restrosofttabsystem.Database;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

public class Common {
    Context context;
    Cursor cursor;
    Database database ;
    public String ip,host,completeip;


    public Common(Context context) {
        this.context = context;
    }

    public String getIpfromDatabase() {
        database = new Database(context,null,null,1);
        List<ConfigIP> get = database.getAllContacts();
        for (ConfigIP cn : get) {
            ip = cn.getIp();
            host = cn.getHost();
        }
        completeip = "http://"+ip+":"+host+"/";

        return completeip;
    }

}
