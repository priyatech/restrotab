package com.abms.restrosofttabsystem.MainModule;

import android.content.Intent;
import android.content.SharedPreferences;
import android.icu.text.Collator;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

//import com.abms.restrosofttabsystem.Asynctask.GetOrderDetails;
import com.abms.restrosofttabsystem.CustomAdapters.CustomActionBar;
import com.abms.restrosofttabsystem.CustomAdapters.CustomAdapter_MenuList;
import com.abms.restrosofttabsystem.CustomAdapters.CustomAdapter_OrderDetails;
import com.abms.restrosofttabsystem.CustomAdapters.GetandSet;
import com.abms.restrosofttabsystem.Database.Common;
import com.abms.restrosofttabsystem.R;
import com.abms.restrosofttabsystem.CustomAdapters.*;
import com.abms.restrosofttabsystem.myAsynkASP.GetSalesItem;


import java.util.ArrayList;

public class ReOrder extends AppCompatActivity {

    Bundle bu ;
    TextView txtTableNo,txtOrderId,txtgrandtotal,txtWaiterId;
    ListView lstReorder;
    ArrayList<GetandSet> temp1;
    CustomAdapter_MenuList customAdapter_menuList;
//    GetOrderDetails getOrderDetails;
    public String orderid,tableno,waiterid;
    Button btnReOrder,btnCancleOrder,btnCheck,btnHome;
    CustomActionBar customActionBar ;
    MyCustomDialog_cancelorder dialog;
    String oid,tid,gtotal,wid;

    // AsynkTask to get Sales Item
    GetSalesItem getSalesItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_re_order);

        String ip = new Common(ReOrder.this).getIpfromDatabase();

        customActionBar = new CustomActionBar(ReOrder.this,getApplication());
        customActionBar.setTxtTitle("Re-Order Table !");
        customActionBar.onimgBackPress(DashBoard.class);

        txtTableNo = findViewById(R.id.txt_RO_TableNo);
        txtOrderId = findViewById(R.id.txt_RO_OrderId);

        txtgrandtotal = findViewById(R.id.txt_RO_GrandTotal);
        txtWaiterId = findViewById(R.id.txt_RO_WaiterId);
        lstReorder = findViewById(R.id.ListView_RO);

        btnReOrder = findViewById(R.id.btn_RO_Reorder);
        btnCancleOrder = findViewById(R.id.btn_RO_CancleOrder);
        btnHome = findViewById(R.id.btn_RO_BackHome);
        bu =getIntent().getExtras();

        SharedPreferences sharedPrefs = getSharedPreferences("com.example.xyz", MODE_PRIVATE);
        Boolean statusCancel = sharedPrefs.getBoolean("strCancelOrder", true);

        Log.i("staus from ipconfig", String.valueOf(statusCancel));
        if (statusCancel== true)
        btnCancleOrder.setVisibility(View.VISIBLE);
        else
            btnCancleOrder.setVisibility(View.GONE);


        if(bu!=null)
        {
            orderid = bu.getString("orderid");
            tableno = bu.getString("tableNo");
            waiterid = bu.getString("waiterid");

            txtTableNo.setText(tableno);
            txtOrderId.setText(orderid);
            txtWaiterId.setText(waiterid);
        }else
        {
            SharedPreferences preferences = getPreferences(MODE_PRIVATE);
            //wname = preferences.getString("waiter_name","0");

            orderid = preferences.getString("Order_Id","0");
            tableno = preferences.getString("Table_No","0");
            waiterid = preferences.getString("waiter_Id","0");

            txtTableNo.setText(tableno);
            txtOrderId.setText(orderid);
            txtWaiterId.setText(waiterid);
        }

        getSalesItem = new GetSalesItem(ReOrder.this,lstReorder,txtgrandtotal,wid,tid);
        getSalesItem.execute(ip+"/"+"Service1.svc"+"/"+ReOrder.this.getString(R.string.getSalesItem)+"/"+orderid);

        btnReOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReOrder.this,NewTable.class);
                    intent.putExtra("table_No",txtTableNo.getText().toString());
                    intent.putExtra("order_id",txtOrderId.getText().toString());
                    intent.putExtra("waiterid",txtWaiterId.getText().toString());
                startActivity(intent);
            }
        });


        btnCancleOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                oid = txtOrderId.getText().toString();
                tid = txtTableNo.getText().toString();
                gtotal = txtgrandtotal.getText().toString();
                wid = txtWaiterId.getText().toString();

                dialog = new MyCustomDialog_cancelorder(oid,tid,gtotal,wid);
                dialog.show(getFragmentManager(), "MyCustomDialog");
            }
        });

        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ReOrder.this,DashBoard.class));
            }
        });

    }

    protected void onPause()
    {
        super.onPause();

        // Store values between instances here
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();  // Put the values from the UI

        String strorid =  txtOrderId.getText().toString();
        String strid   =  txtWaiterId.getText().toString();
        String strtano =  txtTableNo.getText().toString();

        //   editor.putString("waiter_name", strName); // value to store
        editor.putString("waiter_Id", strid); // value to store
        editor.putString("Order_Id", strorid); // value to store
        editor.putString("Table_No", strtano); // value to store

        // Commit to storage
        editor.commit();
    }

    @Override
    public void onBackPressed() {
        Intent startmain = new Intent(ReOrder.this, DashBoard.class);
        startActivity(startmain);
    }

    public String[] getMyData() {
       String tableno,orderid,waiterid;
        tableno =  txtTableNo.getText().toString();
        orderid = txtOrderId.getText().toString();
        waiterid = txtWaiterId.getText().toString();

        String ar[] = new String[3];
        ar[0]=tableno;
        ar[1]=orderid;
        ar[2]=waiterid;
        return ar;
    }
}
