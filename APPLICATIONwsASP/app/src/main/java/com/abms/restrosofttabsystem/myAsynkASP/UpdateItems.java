package com.abms.restrosofttabsystem.myAsynkASP;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.abms.restrosofttabsystem.CustomAdapters.GetandSet;
import com.abms.restrosofttabsystem.MainModule.ReOrder;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class UpdateItems extends AsyncTask<String, String, String> {

    ArrayList<GetandSet> ItemsDetails;
    ArrayList<GetandSet> tempstore = new ArrayList<GetandSet>();

    String m_name,menu_id,rate,desc,total_amount,qty;
    String t_id,w_id,status= null;
    Activity activity;
    String order_id,date;

    public UpdateItems(ArrayList<GetandSet> itemsDetails, String w_id, Activity activity, String order_id, String date, String tableNo) {
        ItemsDetails = itemsDetails;
        this.w_id = w_id;
        this.activity = activity;
        this.order_id = order_id;
        this.date = date;
        this.t_id = tableNo;
    }

    protected void onPreExecute(){}
    protected String doInBackground(String... connUrl){
        HttpURLConnection conn=null;
        BufferedReader reader;

        try{
            final URL url = new URL(connUrl[0]);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setChunkedStreamingMode(0);

            conn.addRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestMethod("POST");

            JSONObject jsonObject = new JSONObject();

            jsonObject.put("date",date);
            jsonObject.put("order_id",order_id);
            jsonObject.put("w_id",w_id);

            for(int i = 0; i < ItemsDetails.size();i++) {

                GetandSet add = new GetandSet();

                menu_id      = ItemsDetails.get(i).getMenu_id().toString();
                qty          = ItemsDetails.get(i).getMenu_qty().toString();
                rate         = ItemsDetails.get(i).getRate().toString();
                total_amount = String.valueOf(ItemsDetails.get(i).getMenu_price());
                m_name       = ItemsDetails.get(i).getM_name().toString();
                desc         = ItemsDetails.get(i).getDes().toString();

                add.setMenu_id(menu_id);
                add.setMenu_qty(qty);
                add.setRate(rate);
                add.setMenu_price(Float.parseFloat(total_amount));
                add.setM_name(m_name);
                add.setDes(desc);

                tempstore.add(add);
            }

            String stringToPost = new Gson().toJson(tempstore);
            jsonObject.put("jsons",stringToPost);

            OutputStream out = new BufferedOutputStream(conn.getOutputStream());
            out.write(jsonObject.toString().getBytes());
            out.flush();
            out.close();

            int result = conn.getResponseCode();
            System.out.println(""+conn.getResponseCode() + ": " + conn.getResponseMessage());
            System.out.println("Response" + " " + result);
            System.out.println(jsonObject.toString());

            if(result==200){
                InputStream in = new BufferedInputStream(conn.getInputStream());
                reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while((line = reader.readLine())!=null){
                    status = line;
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return status;
    }
    protected void onPostExecute(String result){
        super.onPostExecute(result);
        Intent intent = new Intent(activity, ReOrder.class);

        intent.putExtra("tableNo",t_id);
        intent.putExtra("orderid",result);
        intent.putExtra("waiterid",w_id);

        activity.startActivity(intent);
        if(result!=null){
            Toast.makeText(activity,"Items Updated Successfully.",Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(activity,"Doesn't updated.",Toast.LENGTH_LONG).show();
        }
    }
}
