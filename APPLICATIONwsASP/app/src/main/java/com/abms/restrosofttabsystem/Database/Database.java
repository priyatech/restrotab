package com.abms.restrosofttabsystem.Database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.abms.restrosofttabsystem.CustomAdapters.GetandSet;

import java.util.ArrayList;
import java.util.List;

public class Database extends SQLiteOpenHelper {

    private static final String DB_NAME = "restropadIp";
    private static final int DB_VERSION = 1;

    public static final String COL_ID = "ID";

    public static final String TABLE_NAME = "ConfigIp";
    public static final String COL_IP = "IP";
    public static final String COL_HOST = "HOST";

    public static final String TABLE_Login = "Login";
    public static final String COL_W_ID = "w_id";
    public static final String COL_W_NAME = "wname";
    public static final String COL_W_PASSWORD = "password";
    public static final String COL_W_ADDRESS = "address";
    public static final String COL_W_MOBILE = "mobile";

    public static final String TABLE_CATEGORY = "CategoryMaster";
    public static final String COL_CAT_ID = "c_id";
    public static final String COL_CAT_NAME = "c_name";

    public static final String TABLE_MENU = "MenuMaster";
    public static final String COL_m_ID = "m_id";
    public static final String COL_m_NAME = "m_name";
    public static final String COL_m_RATE = "m_rate";
    public static final String COL_m_AC_RATE = "m_Acrate";


    public static final String COL_DBNAME = "DBNAME";
    public static final String COL_SERVER = "DBSERVER";
    public static final String COL_PWD = "DBPWD";



    public SQLiteDatabase DB;

    public Database(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DB_NAME, factory, DB_VERSION);
        Log.d("DatabaseOperation", "Database Created");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        this.DB = db;
            String query = "CREATE TABLE " + TABLE_NAME + " ("
                + COL_IP + " TEXT NOT NULL, "
                + COL_HOST + " INTEGER NOT NULL "
                + ");";
        db.execSQL(query);
        Log.d("DatabaseOperation", "Table ConfigIp Created");


        String queryLogin = "CREATE TABLE " + TABLE_Login + " ("
                + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COL_W_ID + " INTEGER NOT NULL, "
                + COL_W_NAME + " TEXT NOT NULL, "
                + COL_W_PASSWORD + " TEXT NOT NULL, "
                + COL_W_ADDRESS + " TEXT, "
                + COL_W_MOBILE + " TEXT NOT NULL "
                + ");";
        db.execSQL(queryLogin);
        Log.d("DatabaseOperation", "Table Login Created");

        String queryCategory = "CREATE TABLE " + TABLE_CATEGORY + " ("
                + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COL_CAT_ID + " INTEGER NOT NULL, "
                + COL_CAT_NAME + " TEXT NOT NULL "
                + ");";
        db.execSQL(queryCategory);
        Log.d("DatabaseOperation", "Table Category Created");

        String queryMenu = "CREATE TABLE " + TABLE_MENU + " ("
                + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COL_m_ID + " INTEGER NOT NULL, "
                + COL_m_NAME + " TEXT NOT NULL, "
                + COL_m_RATE + " TEXT NOT NULL, "
                + COL_m_AC_RATE + " TEXT "
                + ");";
        db.execSQL(queryMenu);
        Log.d("DatabaseOperation", "TABLE_MENU Created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        this.DB = db;
        db.execSQL(" DROP TABLE IF EXISTS " + TABLE_NAME);//drop tabel command
        onCreate(db);//execute query
    }

    public void addLogin(ConfigIP details)//Adding into Login
    {
        ContentValues values = new ContentValues();
        values.put(COL_W_ID,details.getwId());
        values.put(COL_W_NAME,details.getwName());
        values.put(COL_W_MOBILE,details.getwMobile());
        values.put(COL_W_ADDRESS,details.getwAddress());
        values.put(COL_W_PASSWORD,details.getwPassword());

        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_Login,null,values);
        Log.d("DatabaseOperation","Values Inserted");
        db.close();
    }

    public void addCategory(GetandSet details)//Adding into Login
    {
        ContentValues values = new ContentValues();
        values.put(COL_CAT_ID,details.getCat_id());
        values.put(COL_CAT_NAME,details.getCat_name());

        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_CATEGORY,null,values);
        Log.d("DatabaseOperation","Values Inserted");
        db.close();
    }

    public void addMenu(GetandSet details)//Adding into Login
    {
        ContentValues values = new ContentValues();
        values.put(COL_m_ID,details.getMenu_id());
        values.put(COL_m_NAME,details.getMenu_price());
        values.put(COL_m_RATE,details.getRate());
        values.put(COL_m_AC_RATE,details.getACrate());

        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_MENU,null,values);
        Log.d("DatabaseOperation","Values Inserted");
        db.close();
    }


    public void addDetails(ConfigIP details)//Adding into Login
    {
        ContentValues values = new ContentValues();
        values.put(COL_IP,details.getIp());
        values.put(COL_HOST,details.getHost());

        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_NAME,null,values);
        Log.d("DatabaseOperation","Values Inserted");
        db.close();
    }

    public void updateip(String ip, String host) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_IP,ip);
        contentValues.put(COL_HOST,host);
        db.update(TABLE_NAME, contentValues, null,null);
        db.close();
        Log.d("DatabaseOperation","Values Update");
    }


    public boolean checkipExist()
    {
        boolean flag = false;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = null;
        String SQL = String.format("SELECT * FROM ConfigIp");
        cursor = db.rawQuery(SQL,null);
        if(cursor.getCount()>0)
            flag = true;
        else
            flag = false;
        cursor.close();
        return flag;
    }


    public int getCountCart() {

        int count = 0;
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("SELECT COUNT(*) FROM ConfigIp");
        Cursor cursor = db.rawQuery(query,null);
        if(cursor.moveToFirst())
        {
            do{
                count = cursor.getInt(0);
            }while (cursor.moveToNext());
        }
        //cursor.close();
        return count;
    }

    public Cursor selectQuery(String query) {
        Cursor c1;

        DB = getWritableDatabase();
        c1 = DB.rawQuery(query, null);
        return c1;
    }

    public List<ConfigIP> getAllContacts() {
        List<ConfigIP> reg = new ArrayList<ConfigIP>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        {
            while (cursor.moveToNext()) {
                ConfigIP getset = new ConfigIP();
                getset.setIp(cursor.getString(0));
                getset.setHost(cursor.getString(1));
                reg.add(getset);
            }
            cursor.close();
        }
        return reg;
    }


    public void deleteAllMenu()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_MENU,null,null);
        db.close();
    }


    public void deleteAllCategory()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CATEGORY,null,null);
        db.close();
    }


}


