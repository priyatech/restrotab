    package com.abms.restrosofttabsystem.myAsynkASP;

    import android.annotation.SuppressLint;
    import android.app.Activity;
    import android.app.ProgressDialog;
    import android.content.Context;
    import android.content.Intent;
    import android.os.AsyncTask;
    import android.view.LayoutInflater;
    import android.view.View;
    import android.widget.AdapterView;
    import android.widget.ListView;
    import android.widget.Toast;

    import com.abms.restrosofttabsystem.CustomAdapters.CustomAdapter_ProcessOrder;
    import com.abms.restrosofttabsystem.CustomAdapters.GetandSet;
    import com.abms.restrosofttabsystem.MainModule.DashBoard;
    import com.abms.restrosofttabsystem.MainModule.ReOrder;
    import com.abms.restrosofttabsystem.R;

    import org.json.JSONArray;
    import org.json.JSONObject;

    import java.io.BufferedInputStream;
    import java.io.BufferedReader;
    import java.io.InputStream;
    import java.io.InputStreamReader;
    import java.net.HttpURLConnection;
    import java.net.URL;
    import java.util.ArrayList;
    import java.util.HashMap;
    import java.util.List;

    @SuppressLint("NewApi")
    public class SelectAsynk extends AsyncTask<String, String, String> {

        String status = null;
        Activity activity;
        String userid, orderId,t_id,statusTable;
        String waiterid;
        GetandSet dataDTO;
        ProgressDialog pDialog = null;
        String waiter_name;

        private List<GetandSet> dataList = new ArrayList<>();

        // For checking login authentication
        public SelectAsynk(Activity act) {
            this.activity = act;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(activity);
            pDialog.setMessage("Loading data.."); // typically you will define such
            // strings in a remote file.
            pDialog.show();

        }

        @Override
        protected String doInBackground(String... connUrl) {
            HttpURLConnection conn = null;
            BufferedReader reader;

            try {
                final URL url = new URL(connUrl[0]);
                conn = (HttpURLConnection) url.openConnection();
                conn.addRequestProperty("Content-Type", "application/json; charset=utf-8");
                conn.setRequestMethod("GET");
                int result = conn.getResponseCode();
                if (result == 200) {

                    InputStream in = new BufferedInputStream(conn.getInputStream());
                    reader = new BufferedReader(new InputStreamReader(in));
                    StringBuilder sb = new StringBuilder();
                    String line = null;

                    while ((line = reader.readLine()) != null) {
                        status = line;
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return status;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (pDialog != null && pDialog.isShowing()) {
                pDialog.dismiss();
            }

            if (result != null)
                try {

                    JSONArray jsonArray = new JSONArray(result);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        final String waiter_id = object.getString("W_id");
                        waiter_name = object.getString("W_name");

                        dataDTO = new GetandSet();

                        dataDTO.setWaiterid(waiter_id);
                        waiterid = dataDTO.getWaiterid().toString();
                        System.out.println(waiterid);

                        dataDTO.setWaitername(waiter_name);
                        userid = dataDTO.getWaitername().toString();
                        System.out.println(userid);

                        dataList.add(dataDTO);

                    }
                    // to send data to dashboard class
                    if(waiter_name!=null) {
                        Toast.makeText(activity, "success", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(activity, DashBoard.class);
                        i.putExtra("waitername", userid);
                        i.putExtra("waiterid", waiterid);
                        activity.startActivity(i);

                    }else{
                        Toast.makeText(activity, "Wrong credentials", Toast.LENGTH_SHORT).show();
                    }
        } catch (NullPointerException e)
        {

            Toast toast = new Toast(activity);
            toast.setDuration(Toast.LENGTH_LONG);

            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.your_custom_layout, null);
            toast.setView(view);
            toast.show();


        } catch (Exception ex) {
                    ex.printStackTrace();
                }
            else {
                Toast.makeText(activity, "Could not get any data.", Toast.LENGTH_LONG).show();
            }
        }

        public String wDetails()
    {
        return String.valueOf(dataDTO);
    }
}