package com.abms.restrosofttabsystem.MainModule;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

//import com.abms.restrosofttabsystem.Asynctask.Checklogin;
import com.abms.restrosofttabsystem.CustomAdapters.GetandSet;
import com.abms.restrosofttabsystem.Database.Common;
import com.abms.restrosofttabsystem.Database.ConfigIP;
import com.abms.restrosofttabsystem.Database.Database;
import com.abms.restrosofttabsystem.R;
import com.abms.restrosofttabsystem.myAsynkASP.SelectAsynk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class Login extends AppCompatActivity {
    ImageView imgBack;
    TextView txt_settings;
    Button btn_login;
    public EditText edt_UserName;
    public EditText edt_UserPass;
    String userid, pass, waiterid;
    private List<GetandSet> dataList = new ArrayList<>();
    ProgressDialog progressDialog;

   // Checklogin checklogin;
    NetworkInfo wifiCheck;
    public String ip;

    int count;
    Database database;
    ConfigIP configIP;
    SelectAsynk selectLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edt_UserName = findViewById(R.id.ed_UserName);
        edt_UserPass = findViewById(R.id.ed_UserPass);
        btn_login    = findViewById(R.id.btn_Login);

        database = new Database(getApplicationContext(), null, null, 1);
        configIP = new ConfigIP();

        ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        wifiCheck = connectionManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (wifiCheck.isConnected()) {
            // Do whatever here
            Toast.makeText(getApplicationContext(), "Connected", Toast.LENGTH_SHORT).show();
        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Please Connect to Your Wi-Fi!")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //do things
                            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                            wifiManager.setWifiEnabled(true);
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                userid = edt_UserName.getText().toString();
                pass = edt_UserPass.getText().toString();

                if (edt_UserName.getText().toString().length() == 0) {
                    edt_UserName.setError("Please Enter userName");
                } else if (edt_UserPass.getText().toString().length() == 0) {
                    edt_UserPass.setError("Please Enter Password");
                } else {

                    if(userid.equals("config") && pass.equals("pass@123"))
                    {
                        showaddipdialog();
                        clear();
                    }
                    else
                    {
                        String ip = new Common(Login.this).getIpfromDatabase();

                        //new SelectAsynk(Login.this).execute("http://192.168.1.102:8099/Service1.svc/checkLogin/"+userid+"/"+pass);
                        new SelectAsynk(Login.this).execute(ip+"/"+"Service1.svc"+"/"+Login.this.getString(R.string.checkLogin)+"/"+userid+"/"+pass);
                    }

                }
            }
        });
    }

    private void showaddipdialog() {

        android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(Login.this);
        alertDialog.setTitle("Set Ip");
        alertDialog.setMessage("Please Fill all Information");

        LayoutInflater layoutInflater = this.getLayoutInflater();
        View changePwdLayout = layoutInflater.inflate(R.layout.ipalertdialog, null);

        final EditText edtip, edthost;
        final Switch swtchCancelOrder, swtchCancelItem;
        edtip = changePwdLayout.findViewById(R.id.edt_ip);
        edthost = changePwdLayout.findViewById(R.id.edt_host);
        swtchCancelOrder = changePwdLayout.findViewById(R.id.switchOrder);
        swtchCancelItem = changePwdLayout.findViewById(R.id.switchItem);

        alertDialog.setView(changePwdLayout);

        final Boolean isExist = database.checkipExist();
        if (isExist) {
            List<ConfigIP> get = database.getAllContacts();
            for (ConfigIP cn : get) {
                edtip.setText(cn.getIp());
                edthost.setText(cn.getHost());
            }
        }else
        {
            edtip.setText("");
            edthost.setText("");
        }

            //Set Buttons
            alertDialog.setPositiveButton("Set", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    if (!isExist) {
                        configIP.setIp(edtip.getText().toString());
                        configIP.setHost(edthost.getText().toString());

                        database.addDetails(configIP);
                        Toast.makeText(getApplicationContext(), "Successfully Configured Ip", Toast.LENGTH_LONG).show();
                    } else {
                        database.updateip(edtip.getText().toString(), edthost.getText().toString());
                        Toast.makeText(getApplicationContext(), "Successfully Configured Ip", Toast.LENGTH_LONG).show();
                    }


                        if (swtchCancelOrder.isChecked())
                        {
                            SharedPreferences.Editor editor = getSharedPreferences("com.example.xyz", MODE_PRIVATE).edit();
                            editor.putBoolean("strCancelOrder", true);
                            editor.commit();
                        }
                        else
                        {
                            SharedPreferences.Editor editor = getSharedPreferences("com.example.xyz", MODE_PRIVATE).edit();
                            editor.putBoolean("strCancelOrder", false);
                            editor.commit();
                        }

                    if (swtchCancelItem.isChecked())
                    {
                        SharedPreferences.Editor editor = getSharedPreferences("com.example.xyz", MODE_PRIVATE).edit();
                        editor.putBoolean("strCancelItem", true);
                        editor.commit();
                    }
                    else
                    {
                        SharedPreferences.Editor editor = getSharedPreferences("com.example.xyz", MODE_PRIVATE).edit();
                        editor.putBoolean("strCancelItem", false);
                        editor.commit();
                    }


                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
        }

        @Override
        public void onBackPressed () {
            Intent startmain = new Intent(Intent.ACTION_MAIN);
            startmain.addCategory(Intent.CATEGORY_HOME);
            startmain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startmain);
            //super.onBackPressed();
        }

        public void clear()
        {
            edt_UserName.setText("");
            edt_UserPass.setText("");
        }

}
