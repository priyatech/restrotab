package com.abms.restrosofttabsystem.CustomAdapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.abms.restrosofttabsystem.R;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Ndroid1 on 22/03/2018.
 */

public class CustomAdapter_OrderDetails extends BaseAdapter {

    Activity activity;
    Context CONTEXT;
    LayoutInflater layoutInflater;
    public List<GetandSet> details;
    CustomAdapter_OrderDetails.ViewHolder viewHolder;
    GetandSet getandSet;
    TextView GrandTotal;
    SharedPreferences sharedPrefs ;


    public CustomAdapter_OrderDetails(Activity activity,Context context, List<GetandSet> details, TextView txtGrandtotal) {
        this.activity = activity;
        this.CONTEXT = context;
        this.details = details;
        this.GrandTotal = txtGrandtotal;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public CustomAdapter_OrderDetails() {
    }

    @Override
    public int getCount() {
        return details.size();
    }

    @Override
    public Object getItem(int position) {
        return details.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        getandSet = new GetandSet();

        //final float menu_rate = Integer.parseInt(details.get(position).getRate());
      //  final float menu_rate = Float.parseFloat(details.get(position).getRate());

        sharedPrefs = CONTEXT.getSharedPreferences("com.example.xyz", MODE_PRIVATE);


        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) CONTEXT.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.custom_order_details, null);
            //convertView = layoutInflater.inflate(R.layout.custom_adapter_template, parent, false);
            viewHolder = new CustomAdapter_OrderDetails.ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (CustomAdapter_OrderDetails.ViewHolder) convertView.getTag();
        }

        Boolean statusCancel = sharedPrefs.getBoolean("strCancelItem", true);

        Log.i("strtem from ipconfig", String.valueOf(statusCancel));
        if (statusCancel== true)
            viewHolder.btnEdit.setVisibility(View.VISIBLE);
        else
            viewHolder.btnEdit.setVisibility(View.GONE);


     /*   for (int i = 0; i < details.size(); i++) {
            for (int j = i + 1; j < details.size(); j++) {
                if (details.get(j).getMenu_id().equals(details.get(i).getMenu_id())) {
                    details.remove(j);
                    j--;

                    getandSet = details.get(i);
                    getandSet.Menu_qty = details.get(i).getMenu_qty() + 1;
                }
            }
        }*/
        viewHolder.txtsrno.setText(details.get(position).getMenu_id());
        viewHolder.txtsrno.setVisibility(View.INVISIBLE);
        viewHolder.txtsrnotem.setText(String.valueOf(position+1));
        viewHolder.txtmname.setText(details.get(position).getM_name());
        viewHolder.txtrate.setText(details.get(position).getRate());
        viewHolder.txtqty.setText(String.valueOf(details.get(position).getMenu_qty()));
        viewHolder.txtprice.setText(String.valueOf(details.get(position).getMenu_price()));


      //  String id,menuname,rate,qty,price;

        viewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String id,menuname,rate,qty,price,tableno;
                id       = details.get(position).getMenu_id();
                menuname = details.get(position).getM_name();
                rate     = details.get(position).getRate();
                qty      = String.valueOf(details.get(position).getMenu_qty());
                price    = String.valueOf(details.get(position).getMenu_price());

                MyCustomDialog dialog = new MyCustomDialog(id,menuname,rate,qty,price);
                dialog.show(activity.getFragmentManager(), "MyCustomDialog");
            }
        });

        GrandTotal.setText(String.valueOf(grandTotal(details)));
        return convertView;
    }

    public static class ViewHolder {
        TextView txtsrno, txtmname, txtqty, txtrate, txtprice,txtsrnotem;
       public ImageView btnEdit;
        Context CONTEXT;
        Activity activity;

        public ViewHolder(View view) {
            txtsrno    = view.findViewById(R.id.txt_ROsrno);
            txtsrnotem = view.findViewById(R.id.txt_RO_srno);
            txtmname   = view.findViewById(R.id.txt_RO_Item);
            txtqty     = view.findViewById(R.id.txt_RO_qty);
            txtrate    = view.findViewById(R.id.txt_RO_rate);
            txtprice   = view.findViewById(R.id.txt_RO_price);
            btnEdit   = view.findViewById(R.id.img_edit);

//            SharedPreferences sharedPrefs = getSharedPreferences("com.example.xyz", MODE_PRIVATE);
//             getClass().getSharedPreferences("com.example.xyz", MODE_PRIVATE);

        }
    }

    private String grandTotal(List<GetandSet> items){
        float totalPrice = 0;
        for(int i = 0 ; i < items.size(); i++) {
            totalPrice += items.get(i).getMenu_price();
        }
        return String.valueOf(totalPrice);
    }
}