package com.abms.restrosofttabsystem.CustomAdapters;

import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
//
//import com.abms.restrosofttabsystem.Asynctask.CancleOrder;
import com.abms.restrosofttabsystem.Database.Common;
import com.abms.restrosofttabsystem.MainModule.NewTable;
import com.abms.restrosofttabsystem.MainModule.ReOrder;
import com.abms.restrosofttabsystem.R;
import com.abms.restrosofttabsystem.myAsynkASP.OrderCancelDialogAsynk;

/**
 * Created by Ndroid1 on 28/03/2018.
 */

@SuppressLint("ValidFragment")
public class MyCustomDialog_cancelorder extends DialogFragment {

    private static final String TAG = "MyCustomDialog";

    public interface OnInputListener{
        void sendInput(String input);
    }
    public OnInputListener mOnInputListener;

    //widgets
    private EditText edt_reason;
    private TextView txtorderid, txttableno,txtgrandtotal,txtwaiterid;
    private Button btnclose,btncancleorder;

    String orderid,tableno,grandtotal,waiterid;
    ReOrder reOrder;

//    CancleOrder cancleOrder;

    @SuppressLint("ValidFragment")
    public MyCustomDialog_cancelorder(String orderid, String tableno, String grandtotal, String waiterid) {
        this.orderid = orderid;
        this.tableno = tableno;
        this.grandtotal = grandtotal;
        this.waiterid = waiterid;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialogfragment_ordercancle, container, false);


        txtorderid = view.findViewById(R.id.txt_order_Id1);
        txttableno = view.findViewById(R.id.txt_table_no1);
        txtwaiterid = view.findViewById(R.id.txt_waiter_name);
        txtgrandtotal = view.findViewById(R.id.txt_grand_total);

        edt_reason = view.findViewById(R.id.edt_Reason);

        btnclose = view.findViewById(R.id.btn_close1);
        btncancleorder = view.findViewById(R.id.btn_cancel_order);

        reOrder = (ReOrder) getActivity();

        txtorderid.setText(orderid);
        txtwaiterid.setText(waiterid);
        txttableno.setText(tableno);
        txtgrandtotal.setText(grandtotal);

        btnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: closing dialog");
                getDialog().dismiss();
            }
        });

        btncancleorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String reason = edt_reason.getText().toString();
                final String ip = new Common(reOrder).getIpfromDatabase();
                new OrderCancelDialogAsynk(reOrder,grandtotal,orderid,tableno,waiterid,reason).execute(ip+"Service1.svc"+"/"+reOrder.getString(R.string.deleteOrder));
//
//                cancleOrder = new CancleOrder(reOrder,grandtotal,orderid,tableno,waiterid,reason);
//                cancleOrder.execute();

                Log.d(TAG, "onClick: capturing input");
                getDialog().dismiss();
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            mOnInputListener = (OnInputListener) getActivity();
        }catch (ClassCastException e){
            Log.e(TAG, "onAttach: ClassCastException: " + e.getMessage() );
        }
    }
}
