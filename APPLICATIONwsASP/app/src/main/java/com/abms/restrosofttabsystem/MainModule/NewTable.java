package com.abms.restrosofttabsystem.MainModule;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;
//
//import com.abms.restrosofttabsystem.Asynctask.EmptyTables;
//import com.abms.restrosofttabsystem.Asynctask.GetMenuDetails;
//import com.abms.restrosofttabsystem.Asynctask.InsertSalesITem;
//import com.abms.restrosofttabsystem.Asynctask.InsertTableOrder;
//import com.abms.restrosofttabsystem.Asynctask.KOTPrint;
//import com.abms.restrosofttabsystem.Asynctask.UpdateSalesITem;
import com.abms.restrosofttabsystem.CustomAdapters.CustomActionBar;
import com.abms.restrosofttabsystem.CustomAdapters.CustomAdapter_MenuList;
import com.abms.restrosofttabsystem.CustomAdapters.GetandSet;
import com.abms.restrosofttabsystem.CustomAdapters.SwipeDismissListViewTouchListener;
import com.abms.restrosofttabsystem.Database.Common;
import com.abms.restrosofttabsystem.R;
import com.abms.restrosofttabsystem.myAsynkASP.AllMenuAsynk;
import com.abms.restrosofttabsystem.myAsynkASP.CategoryAsynk;
import com.abms.restrosofttabsystem.myAsynkASP.InsertAsynk;

import com.abms.restrosofttabsystem.myAsynkASP.MenuAsynk;
import com.abms.restrosofttabsystem.myAsynkASP.NewTableAsynk;
import com.abms.restrosofttabsystem.myAsynkASP.UpdateItems;

import java.util.ArrayList;

import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import in.galaxyofandroid.spinerdialog.SpinnerDialog;

public class NewTable extends AppCompatActivity {

    CustomActionBar customActionBar;
    TextClock txtNTTime;
    TextView txtGrandTotal, txtNTdate, txtwaiterid, txtROrderTable, txtTotalItems, txtOrderId;
    EditText edtGuests;

    Spinner spnCategory, spnMenuItem, spnNewTable;
    SpinnerDialog spinnerDialog;

    Button btnAddItem, btnPlaceOrder, btnREOrder, btnSearch;
    ListView LstSelectedItems;
    String tableno, date, time, waiterid, ordertype, tablestatus, txtwaiter, order_id, guest;

    ArrayList<GetandSet> MenuDetails;
    ArrayList<GetandSet> menus;
    ArrayList<GetandSet> temp = new ArrayList<GetandSet>();
    CustomAdapter_MenuList customAdapter_menuList;

    String getCategory;
    String getMenuItem;

    String waiteID, tableNo, OrderID;

    GetandSet getandSet;
    //Asyntasks
    MenuAsynk menuAsynk;
    AllMenuAsynk allMenuAsynk;
    InsertAsynk insertAsynk;
    String tbl_no;
    String ip ;

    Bundle bu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_table);

        ip = new Common(NewTable.this).getIpfromDatabase();


        bu = getIntent().getExtras();
        getandSet = new GetandSet();

        customActionBar = new CustomActionBar(NewTable.this, getApplication());
        customActionBar.setTxtTitle("New Table");
        customActionBar.onimgBackPress(DashBoard.class);

        txtNTTime = findViewById(R.id.txt_NT_time);
        txtNTdate = findViewById(R.id.txt_Nt_Date);
        txtGrandTotal = findViewById(R.id.txt_GrandTotal);
        txtTotalItems = findViewById(R.id.txt_TotalNo);
        txtwaiterid = findViewById(R.id.txtWaiterID);
        txtROrderTable = findViewById(R.id.txt_NTRO_TableNo);
        txtOrderId = findViewById(R.id.txtOrderID);
        edtGuests = findViewById(R.id.edt_guest);

        txtROrderTable.setVisibility(View.INVISIBLE);

        spnCategory = findViewById(R.id.spn_NTCategory);
        spnMenuItem = findViewById(R.id.spn_NTMenuItem);
        spnNewTable = findViewById(R.id.spn_NTselectTable);

        btnAddItem = findViewById(R.id.btn_AddItem);
        btnPlaceOrder = findViewById(R.id.btn_placeorder);
        btnREOrder = findViewById(R.id.btn_ReOrder);
        btnSearch = findViewById(R.id.btn_search);

        btnREOrder.setVisibility(View.INVISIBLE);


        LstSelectedItems = findViewById(R.id.list_SelectedItems);

        if (bu != null) {
            txtwaiter = "Waiter Id :- ";
            waiterid = bu.getString("waiterid");
            txtwaiterid.setText(txtwaiter + waiterid);

            new NewTableAsynk(NewTable.this, spnNewTable).execute(ip + "/" + "Service1.svc" + "/" + NewTable.this.getString(R.string.newTable) + "/" + waiterid);
            new CategoryAsynk(NewTable.this, spnCategory, txtNTdate).execute(ip + "/" + "Service1.svc" + "/" + NewTable.this.getString(R.string.category));


            if (bu.getString("table_No") != null) {
                tableNo = bu.getString("table_No");
                order_id = bu.getString("order_id");

                spnNewTable.setVisibility(View.INVISIBLE);
                txtROrderTable.setVisibility(View.VISIBLE);

                txtROrderTable.setText(tableNo);
                txtOrderId.setVisibility(View.VISIBLE);
                txtOrderId.setText("Order ID : - " + order_id);

                btnPlaceOrder.setVisibility(View.INVISIBLE);
                btnREOrder.setVisibility(View.VISIBLE);
            }

        } else {
            SharedPreferences preferences = getPreferences(MODE_PRIVATE);
            waiterid = preferences.getString("waiter_Id", "0");
            txtwaiter = "Waiter Id :- ";
            txtwaiterid.setText(txtwaiter + waiterid);
        }

//        spnCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                getCategory = spnCategory.getSelectedItem().toString().trim();
//                tableno = spnNewTable.getSelectedItem().toString().trim();
//                if (getCategory.equals("Select Category")) {
//                  //  tableno = spnNewTable.getSelectedItem().toString().trim();
////                    menuAsynk = new MenuAsynk(NewTable.this, spnMenuItem, getCategory, tableno);
////                    menuAsynk.execute(ip + "/" + "Service1.svc" + "/" + NewTable.this.getString(R.string.getMenu) + "/" + getCategory + "/" + tableno);//  +"/"+waiterid
////                    MenuDetails = menuAsynk.details();
//
//                    allMenuAsynk = new AllMenuAsynk(NewTable.this);
//                    allMenuAsynk.execute(ip + "/" + "Service1.svc" + "/" + NewTable.this.getString(R.string.getAllMenu) + "/" + tableno);//  +"/"+waiterid
//                    menus = allMenuAsynk.details();
//
//                } else {
//                    tableno = spnNewTable.getSelectedItem().toString().trim();
//                    menuAsynk = new MenuAsynk(NewTable.this, spnMenuItem, getCategory, tableno);
//                    menuAsynk.execute(ip + "/" + "Service1.svc" + "/" + NewTable.this.getString(R.string.getMenu) + "/" + getCategory + "/" + tableno);
//                    MenuDetails = menuAsynk.details();
//
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//            }
//        });

        spnCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    getCategory = spnCategory.getSelectedItem().toString().trim();
                    tableno = spnNewTable.getSelectedItem().toString().trim();
                    if (getCategory.equals("Select Category"))
                    {
                      tableno = spnNewTable.getSelectedItem().toString().trim();
//                    menuAsynk = new MenuAsynk(NewTable.this, spnMenuItem, getCategory, tableno);
//                    menuAsynk.execute(ip + "/" + "Service1.svc" + "/" + NewTable.this.getString(R.string.getMenu) + "/" + getCategory + "/" + tableno);//  +"/"+waiterid
//                    MenuDetails = menuAsynk.details();
                        spnCategory.setSelection(0);
                        spnMenuItem.setSelection(0);

                        allMenuAsynk = new AllMenuAsynk(NewTable.this);
                        allMenuAsynk.execute(ip + "/" + "Service1.svc" + "/" + NewTable.this.getString(R.string.getAllMenu) + "/" + tableno);//  +"/"+waiterid
                        menus = allMenuAsynk.details();

                    } else {
                        tableno = spnNewTable.getSelectedItem().toString().trim();
                        menuAsynk = new MenuAsynk(NewTable.this, spnMenuItem, getCategory, tableno);
                        menuAsynk.execute(ip + "/" + "Service1.svc" + "/" + NewTable.this.getString(R.string.getMenu) + "/" + getCategory + "/" + tableno);
                        MenuDetails = menuAsynk.details();
                    }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        //Spinner Dialog for Searching
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // for resetting category spinner
                //menus = menu();

                ArrayList<String> menulable = new ArrayList<String>();
                for (int i = 0; i < menus.size(); i++) {
                    menulable.add(menus.get(i).getM_name());
                }
                spinnerDialog = new SpinnerDialog(NewTable.this, menulable, "Select or Search Menu", R.style.DialogAnimations_SmileWindow, "Close");// With 	Animation
                spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
                    @Override
                    public void onClick(String item, int position) {
                        for (int i = 0; i < menus.size(); i++) {
                            if (menus.get(i).getM_name().equals(item)) {
                                GetandSet details = new GetandSet();

                                details.setMenu_id(menus.get(i).getMenu_id());
                                details.setM_name(menus.get(i).getM_name());
                                details.setRate(menus.get(i).getRate());
                                details.setDes(menus.get(i).getDes());

                                temp.add(details);
                            }
                        }

                        customAdapter_menuList = new CustomAdapter_MenuList(NewTable.this, getApplicationContext(), temp, txtGrandTotal, txtTotalItems);
                        LstSelectedItems.setAdapter(customAdapter_menuList);
                        LstSelectedItems.smoothScrollToPosition(customAdapter_menuList.getCount());
                        customAdapter_menuList.notifyDataSetChanged();
                    }
                });
                spinnerDialog.showSpinerDialog();
            }

        });

        btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bu.getString("table_No") != null) {
                    addROBtnClick();
                } else {

                    if (spnNewTable.getSelectedItem().toString().equals("Select New Table")) {
                        Toast toast = Toast.makeText(NewTable.this, "Please Select at Least 1 Table !", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    } else {

                        getMenuItem = spnMenuItem.getSelectedItem().toString().trim();

                        for (int i = 0; i < MenuDetails.size(); i++) {
                            if (MenuDetails.get(i).getM_name().equals(getMenuItem)) {

                                GetandSet details = new GetandSet();

                                details.setMenu_id(MenuDetails.get(i).getMenu_id());
                                details.setM_name(MenuDetails.get(i).getM_name());
                                details.setRate(MenuDetails.get(i).getRate());
                                temp.add(details);
                            }
                        }
                        customAdapter_menuList = new CustomAdapter_MenuList(NewTable.this, getApplicationContext(), temp, txtGrandTotal, txtTotalItems);
                        LstSelectedItems.setAdapter(customAdapter_menuList);
                        customAdapter_menuList.notifyDataSetChanged();
                        spnCategory.setSelection(0);
                        spnMenuItem.setSelection(0);
                    }
                }
            }
        });
        SwipeDismissListViewTouchListener touchListener =
                new SwipeDismissListViewTouchListener(
                        LstSelectedItems,
                        new SwipeDismissListViewTouchListener.DismissCallbacks() {
                            @Override
                            public boolean canDismiss(int position) {
                                return true;
                            }

                            @Override
                            public void onDismiss(ListView listView, int[] reverseSortedPositions) {
                                for (int position : reverseSortedPositions) {
                                    if (reverseSortedPositions.length < 1) {
                                        Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                                    } else {
                                        temp.remove(position);
                                        customAdapter_menuList.notifyDataSetChanged();
                                    }
                                    if (customAdapter_menuList.getCount() < 1) {
                                        txtGrandTotal.setText("0");
                                        txtTotalItems.setText("0");
                                    }
                                }
                                customAdapter_menuList.notifyDataSetChanged();
                            }
                        });
        LstSelectedItems.setOnTouchListener(touchListener);


        btnPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                date = txtNTdate.getText().toString();
                time = txtNTTime.getText().toString();
                guest = edtGuests.getText().toString();
                tableno = spnNewTable.getSelectedItem().toString().trim();
                tablestatus = "Processing";
                ordertype = "Novat";

                if (tableno.equals("Select New Table")) {
                    Toast toast = Toast.makeText(NewTable.this, "Please Select at Least 1 Table !", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (customAdapter_menuList == null) {
                    Toast toast = Toast.makeText(NewTable.this, "Please Select at Least 1 Item !", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (temp.size() != 0) {

                    insertAsynk = new InsertAsynk(NewTable.this, tableno, date, waiterid, ordertype, tablestatus, time, guest, temp); //,temp
                    insertAsynk.execute(ip + "Service1.svc" + "/" + NewTable.this.getString(R.string.addTableOrder));

                } else {
                    Toast toast = Toast.makeText(NewTable.this, "Please Add at Least 1 Item !", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        });

        btnREOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                date = txtNTdate.getText().toString();
                String waiteID, tableNo, time;
                time = txtNTTime.getText().toString();
                waiteID = waiterid;
                tableNo = txtROrderTable.getText().toString();
                if (LstSelectedItems.getCount() == 0 || LstSelectedItems.getCount() < 1) {
                    Toast toast = Toast.makeText(NewTable.this, "Please Select at Least 1 Item !", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (temp.size() != 0) {

                    new UpdateItems(temp, waiterid, NewTable.this, order_id, date, tableNo).execute(ip + "Service1.svc" + "/" + NewTable.this.getString(R.string.UpdateSalesItem));
//                    kotPrint = new KOTPrint(NewTable.this,order_id,tableNo,waiteID,date,time);
//                    kotPrint.execute();

                } else {
                    Toast toast = Toast.makeText(NewTable.this, "Please Add at Least 1 Item !", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        });
    }

    protected void onPause() {
        super.onPause();

        // Store values between instances here
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();  // Put the values from the UI

        //  String strName = txtWaiterName.getText().toString();
        String strid = waiterid;
        String OrderId = order_id;

        //   editor.putString("waiter_name", strName); // value to store
        editor.putString("waiter_Id", strid); // value to store
        editor.putString("order_Id", OrderId); // value to store

        // Commit to storage
        editor.commit();
    }

    @Override
    public void onBackPressed() {
        Intent startmain = new Intent(Intent.ACTION_MAIN);
        startmain.addCategory(Intent.CATEGORY_HOME);
        startmain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startmain);
    }

    public void addROBtnClick() {

        if (spnMenuItem.getSelectedItem().toString().equals("Select Menu Item")) {
            Toast toast = Toast.makeText(NewTable.this, "Please Select at Least 1 Item !", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        } else {

            getMenuItem = spnMenuItem.getSelectedItem().toString().trim();

            for (int i = 0; i < MenuDetails.size(); i++) {
                if (MenuDetails.get(i).getM_name().equals(getMenuItem)) {
                    GetandSet details = new GetandSet();

                    details.setMenu_id(MenuDetails.get(i).getMenu_id());
                    details.setM_name(MenuDetails.get(i).getM_name());
                    details.setRate(MenuDetails.get(i).getRate());

                    temp.add(details);
                }
            }
            customAdapter_menuList = new CustomAdapter_MenuList(NewTable.this, getApplicationContext(), temp, txtGrandTotal, txtTotalItems);
            LstSelectedItems.setAdapter(customAdapter_menuList);
            customAdapter_menuList.notifyDataSetChanged();
        }
    }

}