package com.abms.restrosofttabsystem.CustomAdapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TableRow;
import android.widget.TextView;

//import com.abms.restrosofttabsystem.Asynctask.CancelKOTprinting;
//import com.abms.restrosofttabsystem.Asynctask.CancleUpdateKOT;
import com.abms.restrosofttabsystem.Database.Common;
import com.abms.restrosofttabsystem.MainModule.DashBoard;
import com.abms.restrosofttabsystem.MainModule.Login;
import com.abms.restrosofttabsystem.MainModule.NewTable;
import com.abms.restrosofttabsystem.MainModule.ReOrder;
import com.abms.restrosofttabsystem.R;
import com.abms.restrosofttabsystem.myAsynkASP.UpdateItemQty;
//
//import org.apache.http.entity.mime.content.StringBody;

@SuppressLint("ValidFragment")
public class MyCustomDialog extends DialogFragment {

    private static final String TAG = "MyCustomDialog";
//    CancleUpdateKOT cancleUpdateKOT ;
//    CancelKOTprinting cancelKOTprinting;


    String id,menuname,rate,qty,price,tableno,orderid,waiterid;
    String details[] ;
    int qty1,rate1;
    float amount1;
    int count = 0;

    public MyCustomDialog(String id, String menuname, String rate, String qty, String price) {
        this.id = id;
        this.menuname = menuname;
        this.rate = rate;
        this.qty = qty;
        this.price = price;
    }
    public interface OnInputListener{
        void sendInput(String input);
    }

    public OnInputListener mOnInputListener;

    //widgets
    private TextView txtOrderId, txtTableNo,txtMenuName,txtQty,txtRate,txtAmount,txtdeletedQty;
    private Button btnqtyAdd,btnqtyRemove,btnUpdate,btnCancleItem,btnClose;
    ImageButton imgBtnClose;
    ReOrder activity;
    TableRow tr;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialogfragment, container, false);
        //getDialog().setTitle("Cancel Quantity");

     txtOrderId = view.findViewById(R.id.txt_orderId);
     txtTableNo = view.findViewById(R.id.txt_tableno);
     txtMenuName = view.findViewById(R.id.txt_menu_name);
     txtQty = view.findViewById(R.id.txt_menu_qty);
     txtRate = view.findViewById(R.id.txt_menu_rate);
     txtAmount = view.findViewById(R.id.txt_total_amount);
     txtdeletedQty = view.findViewById(R.id.txtdeletedQty);

     btnqtyAdd = view.findViewById(R.id.btn_qty_add);
     btnqtyRemove = view.findViewById(R.id.btn_qty_remove);
     btnUpdate = view.findViewById(R.id.btn_Update);
     btnClose = view.findViewById(R.id.btn_close);
    // imgBtnClose = view.findViewById(R.id.imgBtnClose);

      tr  = (TableRow) view.findViewById(R.id.tblRowQty);
     //btnCancleItem = view.findViewById(R.id.btn_delete);

//     imgBtnClose.setOnClickListener(new View.OnClickListener() {
//         @Override
//         public void onClick(View v) {
//             Log.d(TAG, "onClick: closing dialog");
//             getDialog().dismiss();
//         }
//     });


        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: closing dialog");
                getDialog().dismiss();
            }
        });

             txtMenuName.setText(menuname);
             txtQty.setText(qty);
             txtRate.setText(rate);
             txtAmount.setText(price);


        activity = (ReOrder) getActivity();
        details = activity.getMyData();

        tableno = details[0].toString();
        orderid = details[1].toString();
        waiterid = details[2].toString();

        txtTableNo.setText(tableno);
        txtOrderId.setText(orderid);


        //Increase Qty
//        btnqtyAdd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                qty1 = Integer.parseInt(txtQty.getText().toString());
//                rate1 = Integer.parseInt(txtRate.getText().toString());
//                amount1 = Float.parseFloat(txtAmount.getText().toString());
//
//                qty1 += 1;
//                amount1 =   rate1 * qty1 ;
//                txtQty.setText(String.valueOf(qty1));
//                txtAmount.setText(String.valueOf(amount1));
//
//            }
//        });
        // Decrease Qty
        btnqtyRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tr.setVisibility(View.VISIBLE);

                qty1 = Integer.parseInt(txtQty.getText().toString());
                rate1 = Integer.parseInt(txtRate.getText().toString());
                amount1 = Float.parseFloat(txtAmount.getText().toString());

                if(qty1>=0)
                {
                    if (qty1 != 0) {
                        qty1 -= 1;

                       // int count=qty1;
                        txtdeletedQty.setText(String.valueOf(count+=1));
                    }
                    amount1 =   rate1 * qty1 ;
                    txtQty.setText(String.valueOf(qty1));
                    txtAmount.setText(String.valueOf(amount1));
                }
            }
        });


        //Update Qty
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Requirement orderId,tblNo,menuName,qty,rate,amount,date,waiterName

              String qty2,rate2,amount2,menuname2,orderid2,tableno2,waiterid2;

                qty2      = txtQty.getText().toString();
                rate2      = txtRate.getText().toString();
                amount2   = txtAmount.getText().toString();
                menuname2 = txtMenuName.getText().toString();
                orderid2  = txtOrderId.getText().toString();
                tableno2  = txtTableNo.getText().toString();
                waiterid2 = waiterid ;

                String update = "update";

                // string order_id, string t_id, string w_id, string m_name, string qty, string rate, string amount, string date, string isOnline
//
                final String ip = new Common(activity).getIpfromDatabase();
                new UpdateItemQty(activity,orderid2,tableno2,waiterid2,menuname2,qty2,rate2,amount2).execute(ip+"/"+"Service1.svc"+"/"+activity.getString(R.string.UpdateSalesItemQty));


//                cancleUpdateKOT = new CancleUpdateKOT(activity,update,id,menuname2,qty2,amount2,orderid2,tableno2,waiterid2);
//                cancleUpdateKOT.execute();

               // cancelKOTprinting = new CancelKOTprinting(activity,orderid2,waiterid2,tableno2,update);
              //  cancelKOTprinting.execute();

            }
        });

        //Cancle Item
//        btnCancleItem.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //Requirement orderId,tblNo,menuName,qty,rate,amount,date,waiterName
//                new AlertDialog.Builder(activity)
//                        .setTitle("Remove Item")
//                        .setMessage("Do you really want to Delete Item?")
//                        .setIcon(android.R.drawable.ic_dialog_alert)
//                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int whichButton) {
//
//                                String qty2,amount2,menuname2,orderid2,tableno2,waiterid2;
//
//                                qty2 =   txtQty.getText().toString();
//                                amount2 =   txtAmount.getText().toString();
//                                menuname2 = txtMenuName.getText().toString();
//                                orderid2 = txtOrderId.getText().toString();
//                                tableno2 = txtTableNo.getText().toString();
//                                waiterid2 = waiterid ;
//
//                                String update = "cancle";
//
////                                cancleUpdateKOT = new CancleUpdateKOT(activity,update,id,menuname2,qty2,amount2,orderid2,tableno2,waiterid2);
////                                cancleUpdateKOT.execute();
//
//                                //cancelKOTprinting = new CancelKOTprinting(activity,orderid2,waiterid2,tableno2,update);
//                               // cancelKOTprinting.execute();
//            }})
//                        .setNegativeButton(android.R.string.no, null).show();
//            }
//        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            mOnInputListener = (OnInputListener) getActivity();
        }catch (ClassCastException e){
            Log.e(TAG, "onAttach: ClassCastException: " + e.getMessage() );
        }
    }
}

