package com.abms.restrosofttabsystem.CustomAdapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.abms.restrosofttabsystem.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ndroid1 on 17/03/2018.
 */

public class CustomAdapter_MenuList extends BaseAdapter{

    Activity activity;
    Context CONTEXT;
    LayoutInflater layoutInflater;
    public List<GetandSet> details;
    CustomAdapter_MenuList.ViewHolder viewHolder;

    GetandSet getandSet;
    TextView GrandTotal;
    TextView txtTotalItems;
    private EditText mInput;
    String desc;

    public CustomAdapter_MenuList(Activity activity,Context context, List<GetandSet> details,TextView txtGrandtotal,TextView txttotalitems) {
        this.activity = activity;
        this.CONTEXT = context;
        this.details = details;
        this.GrandTotal = txtGrandtotal;
        this.txtTotalItems = txttotalitems;
        this.layoutInflater = (LayoutInflater) LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return details.size();
    }

    @Override
    public Object getItem(int position) {
        return details.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        getandSet = new GetandSet();

        final float menu_rate = Float.parseFloat(details.get(position).getRate());
        final int qty = Integer.parseInt(details.get(position).getMenu_qty());


        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater)CONTEXT.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.newtable_template, null);
            viewHolder = new CustomAdapter_MenuList.ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (CustomAdapter_MenuList.ViewHolder) convertView.getTag();
        }

        for(int i = 0 ; i<details.size(); i++)
        {
            for(int j = i+1; j < details.size(); j++)
            {
                if(details.get(j).getMenu_id().equals(details.get(i).getMenu_id()))
                {
                    details.remove(j);
                    j--;

                    getandSet = details.get(i);
                    getandSet.Menu_qty = String.valueOf(Integer.parseInt(details.get(i).getMenu_qty()) + 1);
                }
            }
        }

        viewHolder.txtsrno.setText(details.get(position).getMenu_id());
        viewHolder.txtsrno.setVisibility(View.INVISIBLE);
        viewHolder.txtsrnotem.setText(String.valueOf(position+1));
        viewHolder.txtmname.setText(details.get(position).getM_name());
        viewHolder.txtrate.setText(details.get(position).getRate());

        getandSet = details.get(position);

        getandSet.Menu_price = menu_rate * qty;
        viewHolder.txtqty.setText(getandSet.Menu_qty+"");
        viewHolder.txtprice.setText(getandSet.Menu_price +"");

        GrandTotal.setText(String.valueOf(grandTotal(details)));
        txtTotalItems.setText(String.valueOf(grandTotal(details,position)));

        //notifyDataSetChanged();


        //Add Button Code

        viewHolder.btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getandSet = details.get(position);
                //updateQuantity(position, txtqty,itemrate, 1);
                getandSet.Menu_qty = String.valueOf(qty + 1);
                getandSet.Menu_price = menu_rate * qty;
                viewHolder.txtqty.setText(""+getandSet.Menu_qty);
                viewHolder.txtprice.setText(getandSet.Menu_price +"");

                notifyDataSetChanged();
            }
        });


        //Minus Button
        viewHolder.btnsub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getandSet = details.get(position);
                //updateQuantity(position, txtqty,itemrate, -1);
                if(Integer.parseInt(getandSet.Menu_qty) > 1)
                {
                    getandSet.Menu_qty = String.valueOf(qty - 1);
                    getandSet.Menu_price = menu_rate * qty;
                    viewHolder.txtqty.setText(""+getandSet.Menu_qty);
                    viewHolder.txtprice.setText(getandSet.Menu_price +"");

                    notifyDataSetChanged();
                }
            }
        });

        //Add Description Button
        viewHolder.btn_Desc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog(position);
            }
        });
        viewHolder.txtdesc.setText(details.get(position).getDes().toString());
        notifyDataSetChanged();
        return convertView;
    }

//View Holder Class
    private class ViewHolder {
        TextView txtsrno,txtmname, txtqty,txtrate,txtprice,txtsrnotem,txtdesc;
        Button btnadd,btnsub;
        ImageView btn_Desc;

        public ViewHolder(View view)
        {
            txtsrno = (TextView)view.findViewById(R.id.txt_NTsrno);
            txtsrnotem = (TextView)view.findViewById(R.id.txt_NT_srno);
            txtmname = (TextView)view.findViewById(R.id.txt_NT_Item);
            txtqty = (TextView)view.findViewById(R.id.txt_Nt_qty);
            txtrate = (TextView)view.findViewById(R.id.txt_Nt_rate);
            txtprice = (TextView)view.findViewById(R.id.txt_NT_price);
            txtdesc = (TextView)view.findViewById(R.id.txt_NT_desc);
            btnadd = (Button)view.findViewById(R.id.btn_add);
            btnsub = (Button)view.findViewById(R.id.btn_sub);
            btn_Desc = (ImageView) view.findViewById(R.id.img_desc);
        }
    }

    //Grand - Total
    private String grandTotal(List<GetandSet> items){
        float totalPrice = 0;
        for(int i = 0 ; i < items.size(); i++) {
            totalPrice += items.get(i).getMenu_price();
        }
        return String.valueOf(totalPrice);
    }

    //Number of Items
    private int grandTotal(List<GetandSet> items,int p){
       int totalitems = 0;
        for(int i = 0 ; i < items.size(); i++) {
            totalitems += 1;
        }
        return totalitems;
    }

    private void openDialog(final int position){
        LayoutInflater inflater = LayoutInflater.from(activity);
        View subView = inflater.inflate(R.layout.dialogfragment_description, null);
        mInput = subView.findViewById(R.id.edt_desc);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Add Description");
        builder.setView(subView);
        final AlertDialog alertDialog = builder.create();
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                desc = mInput.getText().toString();
                getandSet = details.get(position);

                details.get(position).setDes(desc);
                notifyDataSetChanged();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });

        builder.show();
    }
}
