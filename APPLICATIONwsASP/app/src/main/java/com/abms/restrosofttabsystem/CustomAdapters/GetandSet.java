package com.abms.restrosofttabsystem.CustomAdapters;

/**
 * Created by Ndroid1 on 13/03/2018.
 */

public class GetandSet {
    String orderid;
    String waiterid;
    String waitername;
    String tableno;
    String ordertime;
    String userid;
    String ACrate;

    public String getACrate() {
        return ACrate;
    }

    public void setACrate(String ACrate) {
        this.ACrate = ACrate;
    }
    // public String LINK = "http://192.168.0.150:8082/";


//    public String getLINK() {
//        return LINK;
//    }

//    public void setLINK(String LINK) {
//        this.LINK = LINK;
//    }

    public String Des = "";

    public String getDes() {
        return Des;
    }

    public void setDes(String des) {
        Des = des;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    String date;

    public GetandSet() {
    }

    public String getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(String menu_id) {
        this.menu_id = menu_id;
    }

    public String getM_name() {
        return m_name;
    }

    public void setM_name(String m_name) {
        this.m_name = m_name;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    String cat_id;

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    String cat_name;
    String menu_id;
    String m_name;
    String rate;

    public String getMenu_qty() {
        return Menu_qty;
    }

    public void setMenu_qty(String menu_qty) {
        Menu_qty = menu_qty;
    }

//    public String getMenu_price() {
//        return Menu_price;
//    }
//
//    public void setMenu_price(String menu_price) {
//        Menu_price = menu_price;
//    }

    String Menu_qty = String.valueOf(1);
    //String Menu_price;
    float Menu_price;

    public float getMenu_price() {
        return Menu_price;
    }

    public void setMenu_price(float menu_price) {
        Menu_price = menu_price;
    }

    //    public int getTotalAmt() {
//        return TotalAmt;
//    }
//
//    public void setTotalAmt(int totalAmt) {
//        TotalAmt = totalAmt;
//    }
//
//    int TotalAmt ;


    String order_id, t_id, status;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getT_id() {
        return t_id;
    }

    public void setT_id(String t_id) {
        this.t_id = t_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    String pass;

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getWaiterid() {
        return waiterid;
    }

    public void setWaiterid(String waiterid) {
        this.waiterid = waiterid;
    }

    public String getWaitername() {
        return waitername;
    }

    public void setWaitername(String waitername) {
        this.waitername = waitername;
    }

    public String getTableno() {
        return tableno;
    }

    public void setTableno(String tableno) {
        this.tableno = tableno;
    }

    public String getOrdertime() {
        return ordertime;
    }

    public void setOrdertime(String ordertime) {
        this.ordertime = ordertime;
    }
}
