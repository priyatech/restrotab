package com.abms.restrosofttabsystem.myAsynkASP;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.abms.restrosofttabsystem.CustomAdapters.GetandSet;
import com.abms.restrosofttabsystem.MainModule.DashBoard;
import com.abms.restrosofttabsystem.MainModule.ReOrder;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class OrderCancelDialogAsynk extends AsyncTask<String, String, String> {
    String status= null;
    Activity activity;

    String order_id,table_no,w_id,amount,Reason;

    public OrderCancelDialogAsynk(Activity activity,String grandtotal,String order_id,String table_no,String w_id,String reason) {
        this.activity = activity;
        this.amount = grandtotal;
        this.order_id = order_id;
        this.w_id =w_id;
        this.table_no = table_no;
        this.Reason = reason;

    }

    protected void onPreExecute(){}
    protected String doInBackground(String... connUrl){
        HttpURLConnection conn=null;
        BufferedReader reader;

        try{
            final URL url = new URL(connUrl[0]);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setChunkedStreamingMode(0);

            conn.addRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestMethod("POST");

            JSONObject jsonObject = new JSONObject();

            jsonObject.put("w_id",w_id);  //object name is case sensEtive. it must be same as service parameter.
            jsonObject.put("order_id",order_id);
            jsonObject.put("t_id",table_no);
            jsonObject.put("reason",Reason);
            jsonObject.put("total_amount",amount);

            OutputStream out = new BufferedOutputStream(conn.getOutputStream());
            out.write(jsonObject.toString().getBytes());
            out.flush();
            out.close();

            int result = conn.getResponseCode();
            System.out.println(""+conn.getResponseCode() + ": " + conn.getResponseMessage());
            System.out.println("Response" + " " + result);
            System.out.println(jsonObject.toString());

            if(result==200){
                InputStream in = new BufferedInputStream(conn.getInputStream());
                reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while((line = reader.readLine())!=null){
                    status = line;
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return status;
    }
    protected void onPostExecute(String result){
        super.onPostExecute(result);

        Intent intent = new Intent(activity, DashBoard.class);
        activity.startActivity(intent);

        if(result!=null){
            Toast.makeText(activity,"Order Cancel Successfuly.",Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(activity,"Doesn't cancel Order.",Toast.LENGTH_LONG).show();
        }
    }

}
