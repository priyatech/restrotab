package com.abms.restrosofttabsystem.CustomAdapters;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.net.sip.SipSession;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.abms.restrosofttabsystem.R;

/**
 * Created by Ndroid1 on 28/03/2018.
 */

public class MyCustomDialog_addDescription extends DialogFragment {

    private static final String TAG = "MyCustomDialog";

    private Listener mListener;

    //widgets
    private EditText mInput;
    private Spinner spndesc;
    Button btnadd,btnclose;



    public void setListener(Listener listener) {
        mListener = listener;
    }

    static interface Listener {
        void returnData(String result);
    }

    //vars

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialogfragment_description, container, false);

        mInput = view.findViewById(R.id.edt_desc);
        spndesc = view.findViewById(R.id.spinner);
//        btnadd = view.findViewById(R.id.btn_addDesc);
//        btnclose = view.findViewById(R.id.btn_closedesc);

        mInput.setText(spndesc.getSelectedItem().toString());

        btnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: closing dialog");
                getDialog().dismiss();
            }
        });

        return view;
    }

    public void onActivityCreated (Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        mInput.setText(spndesc.getSelectedItem().toString().trim());

        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: capturing input");

                String input = mInput.getText().toString();

                if (mListener != null) {
                    mListener.returnData(input);
                }
              dismiss();
            }
        });
    }
}
