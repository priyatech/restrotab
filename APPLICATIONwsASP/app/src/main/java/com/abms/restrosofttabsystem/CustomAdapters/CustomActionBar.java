package com.abms.restrosofttabsystem.CustomAdapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.abms.restrosofttabsystem.R;

/**
 * Created by Ndroid1 on 15/03/2018.
 */

public class CustomActionBar extends Activity {
    public TextView txtTitle;
    Activity activity;
    final Context context;
    public ImageView imgback;
    Class<?> Class ;

    View view;
    public CustomActionBar(Activity act,Context con) {
      this.activity = act;
      this.context = con;

    //  activity.setContentView(R.layout.custom_action_bar);

      imgback = (ImageView)activity.findViewById(R.id.img_back);
      txtTitle = (TextView)activity.findViewById(R.id.txt_title);
    }
    public void setTxtTitle(String s)
    {
        txtTitle.setText(s);
    }
    public void onimgBackPress(Class<?> c)
    {
        this.Class = c ;
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity,Class);
                activity.startActivity(intent);

                //activity.onBackPressed();
            }
        });
    }
    public void onimgBackPress()
    {
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onBackPressed();
            }
        });
    }
}
