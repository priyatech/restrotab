package com.abms.restrosofttabsystem.myAsynkASP;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.abms.restrosofttabsystem.CustomAdapters.GetandSet;
import com.abms.restrosofttabsystem.MainModule.ReOrder;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class UpdateItemQty extends AsyncTask<String, String, String> {

    String m_name,qty,w_id,status= null,order_id, t_id, rate, totalAmt;
    Activity activity;


    //activity,orderid2,tableno2,waiterid2,menuname2,qty2,rate2,amount2

    public UpdateItemQty(Activity activity,String order_id, String t_id, String w_id, String m_name, String qty, String rate, String totalAmt) {
        this.activity = activity;
        this.order_id = order_id;
        this.t_id = t_id;
        this.w_id = w_id;
        this.m_name = m_name;
        this.qty = qty;
        this.rate = rate;
        this.totalAmt = totalAmt;
    }

    protected void onPreExecute(){}
    protected String doInBackground(String... connUrl){
        HttpURLConnection conn=null;
        BufferedReader reader;

        try{
            final URL url = new URL(connUrl[0]);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setChunkedStreamingMode(0);

            conn.addRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestMethod("POST");

            JSONObject jsonObject = new JSONObject();

            //string order_id, string t_id, string w_id, string m_name, string qty, string rate, string amount, string isOnline
            jsonObject.put("qty",qty);
            jsonObject.put("m_name",m_name);
            jsonObject.put("order_id",order_id);
            jsonObject.put("w_id",w_id);
            jsonObject.put("t_id",t_id);
            jsonObject.put("rate",rate);
            jsonObject.put("amount",totalAmt);

            OutputStream out = new BufferedOutputStream(conn.getOutputStream());
            out.write(jsonObject.toString().getBytes());
            out.flush();
            out.close();

            int result = conn.getResponseCode();
            System.out.println(""+conn.getResponseCode() + ": " + conn.getResponseMessage());
            System.out.println("Response" + " " + result);
            System.out.println(jsonObject.toString());

            if(result==200){
                InputStream in = new BufferedInputStream(conn.getInputStream());
                reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while((line = reader.readLine())!=null){
                    status = line;
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return status;
    }
    protected void onPostExecute(String result){
        super.onPostExecute(result);
        Intent intent = new Intent(activity, ReOrder.class);
        activity.startActivity(intent);
        if(result!=null){
            Toast.makeText(activity,"Items Updated Successfully.",Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(activity,"Cannot update.",Toast.LENGTH_LONG).show();
        }
    }
}
