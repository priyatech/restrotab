package com.abms.restrosofttabsystem.myAsynkASP;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.abms.restrosofttabsystem.CustomAdapters.GetandSet;
import com.abms.restrosofttabsystem.MainModule.DashBoard;
import com.abms.restrosofttabsystem.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class NewTableAsynk extends AsyncTask<String, String, String> {

    String status = null;
    Activity activity;
    String t_id;
    String waiterid;
    GetandSet dataDTO;
    ProgressDialog pDialog = null;
    Spinner spnnewtable;

    private List<GetandSet> dataList = new ArrayList<>();

    // For checking login authentication
    public NewTableAsynk(Activity act, Spinner newtablespinner) {
        this.activity = act;
        this.spnnewtable = newtablespinner;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Loading data.."); // typically you will define such
        // strings in a remote file.
        pDialog.show();
    }

    @Override
    protected String doInBackground(String... connUrl) {
        HttpURLConnection conn = null;
        BufferedReader reader;

        try {
            final URL url = new URL(connUrl[0]);
            conn = (HttpURLConnection) url.openConnection();
            conn.addRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestMethod("GET");
            int result = conn.getResponseCode();
            if (result == 200) {

                InputStream in = new BufferedInputStream(conn.getInputStream());
                reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    status = line;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return status;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }

        if (result != null)
            try {

                JSONArray jsonArray = new JSONArray(result);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    final String table_id = object.getString("T_id");

                    dataDTO = new GetandSet();
                        dataDTO.setT_id(table_id);
                    dataList.add(dataDTO);

                }

                List<String> labels = new ArrayList<String>();
                labels.add("Select New Table");
                for(int i = 0; i<dataList.size(); i++)
                {
                    labels.add(dataList.get(i).getT_id().toString());
                }

                ArrayAdapter<String> adp1 = new ArrayAdapter<String>
                        (activity,R.layout.spinner_item,labels);
                adp1.setDropDownViewResource(R.layout.spinner_dropdown_item);
                spnnewtable.setAdapter(adp1); // spn new table



            } catch (NullPointerException e){
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        else {
            Toast.makeText(activity, "Could not get table data.", Toast.LENGTH_LONG).show();
        }
    }

    public String wDetails()
    {
        return String.valueOf(dataDTO);
    }
}